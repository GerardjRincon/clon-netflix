//import liraries
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Platform,
  Animated,
  useWindowDimensions,
} from 'react-native';
import {BlurView} from '@react-native-community/blur';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

//// variables de entorno
import {API_KEY} from '@env';

//// components
import CardDefault from './components/cardDefault';
import ListaNetflix from './components/listNetflix';

// create a component
const Home = () => {
  const ios = Platform.OS === 'ios' ? true : false;
  const URLS = [
    {
      id: 2,
      title: 'Tendencias',
      url: `https://api.themoviedb.org/3/trending/all/day?api_key=${API_KEY}&append_to_response=images&page=1`,
      type: 1,
    },
    {
      id: 3,
      title: 'Solo TV',
      url: `https://api.themoviedb.org/3/discover/tv?api_key=${API_KEY}&append_to_response=images&page=1`,
      type: 1,
    },
    {
      id: 4,
      title: 'Coleccion de rocky',
      url: `https://api.themoviedb.org/3/search/collection?api_key=${API_KEY}&query=rocky&page=1&append_to_response=images`,
      type: 2,
    },
  ];

  const [loading, setLoading] = useState(false); /// loader de vista
  const [itemDefault, setItemDefault] = useState({}); /// item por defecto
  const [cargo, setCargo] = useState(false);

  /// // constantes de animacion
  const pan = React.useRef(new Animated.Value(0)).current; // const de animacion de scroll
  const BANNER_H = 150; // tamaño de banner de la imagen
  /// ref de animacion de carousel
  const scrollXCarousel = React.useRef(new Animated.Value(0)).current;
  const slidesRef = React.useRef(null);
  const {width} = useWindowDimensions();

  useEffect(() => {
    getDefault();
  }, []);

  const getDefault = async () => {
    setLoading(true);
    await fetch(
      `https://api.themoviedb.org/3/movie/157336?api_key=${API_KEY}&append_to_response=images`,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      },
    )
      .then(res => res.json())
      .then(res => {
        // console.warn(res);
        setItemDefault(res);
        setLoading(false);
      })
      .catch(error => {
        console.warn(error);
        setLoading(false);
      });
  };

  const ItemsCategories = ({title, style, right}) => {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={() => console.warn(title)}
        style={[
          style,
          {
            padding: 5,
            paddingHorizontal: 10,
            borderRadius: 20,
            borderWidth: 1,
            height: hp('4'),
            borderColor: 'white',
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: right ? 'row' : 'column',
          },
        ]}>
        <Text style={{color: 'white', fontSize: hp('1.6%')}}>{title}</Text>
        {right ? (
          <Icon name="chevron-down" size={hp('2')} color="white" />
        ) : null}
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <Animated.View style={styles.topBar(pan, BANNER_H, ios)}>
        <View style={{width: '100%', height: 55, paddingHorizontal: 20}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: ios ? 50 : 30,
              height: 55,
              paddingVertical: 15,
            }}>
            <View style={{justifyContent: 'flex-start'}}>
              <Text
                style={{
                  fontSize: hp('2.5'),
                  color: 'white',
                  fontWeight: '700',
                }}>
                Para Gerard Josue
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  marginRight: 15,
                  justifyContent: 'flex-start',
                }}>
                <TouchableOpacity
                  style={{marginRight: 15}}
                  activeOpacity={0.9}
                  onPress={() => console.warn('compartir pantalla')}>
                  <Icon
                    name="video-wireless-outline"
                    size={hp('3%')}
                    color="white"
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  activeOpacity={0.9}
                  onPress={() => console.warn('buscar')}>
                  <Icon name="magnify" size={hp('3%')} color="white" />
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                activeOpacity={0.9}
                onPress={() => console.warn('cuenta')}
                style={{marginRight: 5}}>
                <Icon name="account" size={hp('3%')} color="white" />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <Animated.View style={styles.categories(pan, BANNER_H)}>
          <ItemsCategories title={'Series'} />
          <ItemsCategories style={{marginLeft: 10}} title={'Películas'} />
          <ItemsCategories
            style={{marginLeft: 10}}
            title={'Categorías'}
            right
          />
        </Animated.View>
      </Animated.View>
      <View style={{flex: 1, paddingHorizontal: 20}}>
        <Animated.ScrollView
          onScroll={Animated.event([{nativeEvent: {contentOffset: {y: pan}}}], {
            useNativeDriver: false,
          })}
          style={{flex: 1, marginBottom: 85}}
          showsVerticalScrollIndicator={false}>
          <CardDefault itemDefault={itemDefault} />
          {URLS.map((item, index) => (
            <ListaNetflix
              key={index}
              id={item.id}
              url={item.url}
              title={item.title}
            />
          ))}
        </Animated.ScrollView>
      </View>
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
  },
  topBar: (pan, BANNER_H, ios) => ({
    zIndex: 1000,
    height: pan.interpolate({
      inputRange: ios ? [-105, 105] : [-105, 100],
      outputRange: ios ? [150, 105] : [150, 100],
      extrapolateRight: 'clamp',
    }),
    backgroundColor: pan.interpolate({
      inputRange: [0, 330],
      outputRange: ['rgba(32, 32, 32, 0)', 'rgba(32,32,32, 1)'],
    }),
    transform: [
      {
        translateY: pan.interpolate({
          inputRange: [-BANNER_H, 0],
          outputRange: [0, 1],
        }),
      },
    ],
  }),
  categories: (pan, BANNER_H) => ({
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    zIndex: 10,
    opacity: pan.interpolate({
      inputRange: [0, 30],
      outputRange: [1, 0],
      extrapolateRight: 'clamp',
    }),
    transform: [
      {
        translateY: pan.interpolate({
          inputRange: [-10, 100],
          outputRange: [50, -100],
          extrapolateRight: 'clamp',
        }),
      },
    ],
  }),
});

//make this component available to the app
export default Home;
