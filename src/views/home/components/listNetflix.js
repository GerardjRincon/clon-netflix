//import liraries
import React, {useEffect, useState} from 'react';
import {View, Text, FlatList, Image, Modal} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
//// variables de entorno
import {API_KEY} from '@env';
import {TouchableOpacity} from 'react-native-gesture-handler';
import ModalDetails from './modalDetails';

// create a component
const ListaNetflix = ({url, title, id}) => {
  ///// variables de componente
  const [list, setList] = useState({}); /// objeto de la consulta
  const [loading, setLoading] = useState(false); /// loader de la consulta
  const [visible, setVisible] = useState(false); /// modal status
  const [itemDetails, setItemDetails] = useState({}); /// objeto de la seleccion

  useEffect(() => {
    getLista();
  }, []);

  const getLista = async () => {
    setLoading(true);
    await fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(res => res.json())
      .then(res => {
        setList(res.results);
        setLoading(false);
      })
      .catch(error => {
        console.warn(error);
        setLoading(false);
      });
  };

  const getDetails = async id => {
    setLoading(true);
    await fetch(`https://api.themoviedb.org/3/movie/${id}?api_key=${API_KEY}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(res => res.json())
      .then(res => {
        console.warn(res);
        setItemDetails(res);
        setVisible(true);
        setLoading(false);
      })
      .catch(error => {
        console.warn(error);
        setLoading(false);
      });
  };

  const itemRender_ = ({item}) => {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={() => getDetails(item.id)}
        style={{
          width: 100,
          height: 150,
          borderRadius: 10,
          marginHorizontal: 10,
        }}>
        <Image
          source={{uri: 'https://image.tmdb.org/t/p/w500/' + item.poster_path}}
          resizeMode="cover"
          style={{
            width: '100%',
            height: '100%',
            position: 'absolute',
            zIndex: -1,
          }}
        />
      </TouchableOpacity>
    );
  };

  return (
    <View style={{marginTop: 20, marginBottom: 20}}>
      <Text style={{color: 'white', fontSize: hp('2'), fontWeight: '600'}}>
        {title}
      </Text>
      <FlatList
        data={list}
        renderItem={itemRender_}
        keyExtractor={(item, i) => i}
        horizontal
        //   getItemLayout={(data, index) => getItemLayout(data, index)}
        showsHorizontalScrollIndicator={false}
        style={{flex: 1, marginTop: 10}}
      />
      <ModalDetails
        visible={visible}
        itemDetails={itemDetails}
        onClose={() => setVisible(false)}
      />
    </View>
  );
};

//make this component available to the app
export default ListaNetflix;
