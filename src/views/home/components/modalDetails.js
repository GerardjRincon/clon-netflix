//import liraries
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Modal,
  TouchableOpacity,
  Image,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import LinearGradient from 'react-native-linear-gradient';

// create a component
const ModalDetails = props => {
  const [visible, setVisible] = useState(false); /// estatus de modal
  const date = new Date(props.itemDetails.release_date);

  useEffect(() => {
    setVisible(props.visible);
    console.warn(props.itemDetails);
  }, [props.visible]);

  const onClose = () => {
    setVisible(false);
    props.onClose();
  };

  return (
    <Modal animationType="slider" transparent={true} visible={visible}>
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => onClose()}
        style={{
          flex: 1,
          justifyContent: 'flex-end',
        }}>
        <TouchableOpacity
          activeOpacity={1}
          style={{
            height: hp('30%'),
            width: wp('100%'),
            backgroundColor: '#393939',
            borderTopLeftRadius: 5,
            borderTopRightRadius: 5,
          }}>
          <View
            style={{
              flex: 1,
              marginTop: 20,
            }}>
            <View
              style={{
                flexDirection: 'row',
                paddingHorizontal: 10,
              }}>
              <Image
                source={{
                  uri:
                    'https://image.tmdb.org/t/p/w500/' +
                    props.itemDetails.poster_path,
                }}
                resizeMode="cover"
                style={{width: 100, height: 130, borderRadius: 5}}
              />
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View
                  style={{
                    marginLeft: 10,
                    flex: 1,
                  }}>
                  <Text
                    numberOfLines={1}
                    style={{
                      width: '85%',
                      color: 'white',
                      fontSize: hp('2%'),
                      fontWeight: '600',
                      alignItems: 'flex-start',
                    }}>
                    {props.itemDetails.original_title}
                  </Text>
                  <View style={{flexDirection: 'row', marginTop: 5}}>
                    <Text style={{color: '#656565', fontSize: 16}}>
                      {date.getFullYear()} -
                    </Text>
                    <Text
                      style={{color: '#656565', fontSize: 16, marginLeft: 10}}>
                      language "{props.itemDetails.original_language}" -
                    </Text>
                    <Text
                      style={{color: '#656565', fontSize: 16, marginLeft: 10}}>
                      {props.itemDetails.genres?.length} categories
                    </Text>
                  </View>
                  <View style={{flex: 1}}>
                    <Text style={{color: 'white', fontSize: 16}}>
                      {props.itemDetails.overview}
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    alignItems: 'flex-end',
                    marginRight: 10,
                  }}>
                  {/* <Text numberOfLines={1} style={{fontSize:hp('1.7%'), width:'40%', zIndex: 10, opacity:0.7, fontWeight:'bold'}}>Acciones: {props.perfil.nombre_perfil}</Text> */}
                  <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() => onClose()}
                    style={{
                      backgroundColor: '#555555',
                      borderRadius: 50,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Icon name="close" size={hp('3%')} color="white" />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <View style={{flex: 1}}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingHorizontal: 20,
                  marginTop: 10,
                }}>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <View
                    style={{
                      backgroundColor: '#555555',
                      borderRadius: 50,
                      padding: 3,
                      marginBottom: 5,
                    }}>
                    <Icon name="play" color="white" size={hp('3')} />
                  </View>
                  <Text style={{color: 'white', fontSize: 20}}>Play</Text>
                </View>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <View
                    style={{
                      backgroundColor: '#555555',
                      borderRadius: 50,
                      padding: 3,
                      marginBottom: 5,
                    }}>
                    <Icon name="download" color="white" size={hp('3')} />
                  </View>
                  <Text style={{color: 'white', fontSize: 20}}>Download</Text>
                </View>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <View
                    style={{
                      backgroundColor: '#555555',
                      borderRadius: 50,
                      padding: 3,
                      marginBottom: 5,
                    }}>
                    <Icon name="plus" color="white" size={hp('3')} />
                  </View>
                  <Text style={{color: 'white', fontSize: 20}}>My List</Text>
                </View>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <View
                    style={{
                      backgroundColor: '#555555',
                      borderRadius: 50,
                      padding: 3,
                      marginBottom: 5,
                    }}>
                    <Icon name="share" color="white" size={hp('3')} />
                  </View>
                  <Text style={{color: 'white', fontSize: 20}}>Share</Text>
                </View>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      </TouchableOpacity>
    </Modal>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2c3e50',
  },
});

//make this component available to the app
export default ModalDetails;
