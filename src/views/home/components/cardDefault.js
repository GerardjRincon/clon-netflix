//import liraries
import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image, Platform} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import LinearGradient from 'react-native-linear-gradient';

// create a component
const CardDefault = ({itemDefault}) => {
  const ItemGender = ({item, index}) => {
    return (
      <View
        key={index}
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        {index % 3 ? (
          <View
            style={{
              width: 3,
              height: 3,
              backgroundColor: 'red',
              borderRadius: 10,
            }}
          />
        ) : null}
        <Text
          style={{
            color: 'white',
            marginHorizontal: 10,
            fontSize: hp('1.6'),
          }}>
          {item}
        </Text>
      </View>
    );
  };

  return (
    <View style={{paddingHorizontal: 10}}>
      <View
        style={{
          width: '100%',
          marginTop: 10,
          borderRadius: 10,
          height: hp('55%'),
        }}>
        <Image
          source={{
            uri: 'https://image.tmdb.org/t/p/w500/' + itemDefault.poster_path,
          }}
          resizeMode="cover"
          style={{
            width: '100%',
            position: 'absolute',
            height: '100%',
            borderRadius: 10,
          }}
        />
        <LinearGradient
          colors={[
            'rgba(0, 0, 0, 0)',
            'rgba(0, 0, 0, 0.1)',
            'rgba(0, 0, 0, 0.9)',
          ]}
          style={{flex: 1, borderRadius: 10}}>
          <View style={{flex: 1, justifyContent: 'flex-end'}}>
            <View
              style={{
                height: 150,
                width: '100%',
              }}>
              <View style={{alignItems: 'center', marginBottom: 10}}>
                <Text
                  style={{
                    color: 'white',
                    fontSize: hp('3.5%'),
                    fontWeight: '600',
                  }}>
                  {itemDefault.title}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  paddingHorizontal: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                {itemDefault.genres?.slice(0, 3).map((item, index) => (
                  <ItemGender key={index} item={item.name} index={index} />
                ))}
              </View>
              <View
                style={{
                  width: '100%',
                  marginTop: 15,
                  flexDirection: 'row',
                  paddingHorizontal: 20,
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  style={{
                    backgroundColor: 'white',
                    padding: 5,
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 5,
                    width: '45%',
                  }}>
                  <Icon name="play" size={hp('2.5')} color="black" />
                  <Text style={{color: 'black', fontSize: hp('2')}}>
                    Reproducir
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    backgroundColor: '#333',
                    padding: 5,
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 5,
                    width: '45%',
                  }}>
                  <Icon name="plus" size={hp('2.5')} color="white" />
                  <Text style={{color: 'white', fontSize: hp('2')}}>
                    Mi lista
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </LinearGradient>
      </View>
    </View>
  );
};

//make this component available to the app
export default CardDefault;
