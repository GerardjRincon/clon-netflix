//import liraries
import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Platform,
  TouchableOpacity,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {
  Stack,
  TextInput,
  IconButton,
  Button,
  Divider,
} from '@react-native-material/core';
import CheckBox from '@react-native-community/checkbox';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

// create a component
const Login = ({navigation}) => {
  const [password, setPassword] = useState(''); /// para mostrar contraseña
  const [email, setEmail] = useState('');
  const [viewPassword, setViewPassword] = useState(false);
  const [disabledButton, setDisabledButton] = useState(false);
  const [checkbox, setCheckbox] = useState(false);

  const SignIn = () => {
    navigation.navigate('Home');
  };

  return (
    <View>
      <Image
        source={require('../../assets/img/background.jpeg')}
        resizeMode="cover"
        style={styles.absolute}
      />
      <View style={[styles.absolute, {backgroundColor: 'rgba(0,0,0,0.7)'}]} />
      <View
        style={{
          width: '100%',
          height: '100%',
          justifyContent: 'center',
        }}>
        <View
          style={{
            height: '50%',
            justifyContent: 'flex-start',
            paddingHorizontal: 50,
            paddingVertical: 20,
          }}>
          <Text style={{color: 'white', fontSize: hp(4.5), fontWeight: '700'}}>
            Sign In
          </Text>
          <Stack spacing={10} style={styles.stackInputs}>
            <View>
              <TextInput
                placeholder="Email or phone number"
                style={styles.inputs}
                value={email}
                inputContainerStyle={{backgroundColor: '#333'}}
                inputStyle={{
                  paddingTop: 10,
                  paddingLeft: 15,
                  color: 'white',
                }}
                color="#e87c03"
                onChangeText={e => setEmail(e)}
              />
            </View>
            <View>
              <TextInput
                placeholder="Password"
                value={password}
                style={styles.inputs}
                color="#e87c03"
                inputContainerStyle={{backgroundColor: '#333'}}
                inputStyle={{
                  paddingTop: 10,
                  paddingLeft: 15,
                }}
                secureTextEntry={viewPassword}
                onChangeText={e => setPassword(e)}
                trailing={props => (
                  <IconButton
                    onPress={() =>
                      setViewPassword(viewPassword => !viewPassword)
                    }
                    color="white"
                    icon={props =>
                      viewPassword ? (
                        <Icon name="eye" {...props} color="white" />
                      ) : (
                        <Icon name="eye-off" {...props} color="white" />
                      )
                    }
                    {...props}
                  />
                )}
              />
            </View>
          </Stack>
          <TouchableOpacity
            activeOpacity={0.9}
            onPress={() => SignIn()}
            onLongPress={() => SignIn()}
            style={{
              height: 50,
              marginTop: 20,
              marginLeft: -5,
              width: '104%',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#E50914',
            }}>
            <Text
              style={{fontSize: hp(1.8), color: 'white', fontWeight: '700'}}>
              Sign In
            </Text>
          </TouchableOpacity>
          <View
            style={{
              marginTop: 20,
              flexDirection: 'row',
              width: '100%',
              alignItems: 'center',
              justifyContent: 'space-around',
              paddingHorizontal: 25,
            }}>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <CheckBox
                value={checkbox}
                onValueChange={newValue => setCheckbox(newValue)}
                tintColors={{true: '#E50914', false: '#666666'}}
                style={styles.checkbox}
              />
              <View
                style={{
                  width: '100%',
                  marginLeft: 0,
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    width: '90%',
                    fontSize: hp('1.5%'),
                    color: 'white',
                  }}>
                  Remember me
                </Text>
              </View>
            </View>
            <TouchableOpacity
              activeOpacity={0.9}
              onPress={() => console.warn('necesito ayuda')}>
              <Text style={{color: 'white'}}>Need help?</Text>
            </TouchableOpacity>
          </View>
          <View style={{paddingHorizontal: 50, marginTop: 40}}>
            <View
              style={{
                alignItems: 'center',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text
                  style={{color: '#737373', marginRight: 5, fontSize: hp('2')}}>
                  New to Netflix?
                </Text>
                <TouchableOpacity
                  activeOpacity={0.9}
                  onPress={() => console.warn('Registro con email')}>
                  <Text style={{color: 'white', fontSize: hp('2')}}>
                    Sign up now
                  </Text>
                </TouchableOpacity>
              </View>
              <View>
                <Text
                  style={{
                    color: 'white',
                    fontSize: hp('2'),
                    fontWeight: '600',
                    marginVertical: 10,
                  }}>
                  Or
                </Text>
              </View>
              <TouchableOpacity
                activeOpacity={0.9}
                onPress={() => console.warn('login con facebook')}
                style={{
                  backgroundColor: '#3b5998',
                  flexDirection: 'row',
                  alignItems: 'center',
                  paddingHorizontal: 10,
                  paddingVertical: 5,
                  borderRadius: 5,
                }}>
                <View
                  style={{
                    backgroundColor: 'white',
                    borderRadius: hp('3'),
                    marginRight: 10,
                  }}>
                  <Icon name="facebook" color="#3b5998" size={hp('3')} />
                </View>
                <Text style={{color: 'white', fontSize: hp('2')}}>
                  Login with facebook
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  absolute: {
    position: 'absolute',
    width: wp('100%'),
    height: hp('100%'),
  },
  stackInputs: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  inputs: {
    width: wp('80%'),
  },
  checkbox: {
    width: 25,
    height: 25,
    marginRight: 10,
  },
});

//make this component available to the app
export default Login;
