//import liraries
import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import NavigatorContain from './router/navigatorContainer';

// create a component
const App = () => {
  return <NavigatorContain />;
};

//make this component available to the app
export default App;
