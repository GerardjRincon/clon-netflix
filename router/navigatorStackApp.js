//import liraries
import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import {
  HOME, // inicio
  NEW, // nuevos estrenos
  LAUGHS, // risas
  DOWNLOAD, // descargas
} from './router';

// instancia de la navegacion
const Stack = createNativeStackNavigator();

//// views
import Home from '../src/views/home';
import New from '../src/views/new';
import Laughs from '../src/views/laughs';
import Download from '../src/views/download';

/// navegacion de home
const Home_stack = () => {
  return (
    <Stack.Navigator initialRouteName={HOME}>
      <Stack.Screen
        name={HOME}
        component={Home}
        options={{
          headerShown: false,
          bottomShown: false,
        }}
      />
    </Stack.Navigator>
  );
};

const New_stack = () => {
  return (
    <Stack.Navigator initialRouteName={NEW}>
      <Stack.Screen
        name={NEW}
        component={New}
        options={{
          headerShown: false,
          bottomShown: false,
        }}
      />
    </Stack.Navigator>
  );
};

const Laughs_stack = () => {
  return (
    <Stack.Navigator initialRouteName={LAUGHS}>
      <Stack.Screen
        name={LAUGHS}
        component={Laughs}
        options={{
          title: 'Laughs',
          headerShown: false,
          bottomShown: false,
        }}
      />
    </Stack.Navigator>
  );
};

const Download_stack = () => {
  return (
    <Stack.Navigator initialRouteName={DOWNLOAD}>
      <Stack.Screen
        name={DOWNLOAD}
        component={Download}
        options={{
          title: 'Download',
          headerShown: false,
          bottomShown: false,
        }}
      />
    </Stack.Navigator>
  );
};

/// export de stacks navigators
export {Home_stack, New_stack, Laughs_stack, Download_stack};
