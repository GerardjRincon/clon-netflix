//import liraries
import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

// instancia de navigator
const Stack = createNativeStackNavigator();

//// views
import Login from '../src/views/login';
import Bottom from './navigatorBottom';

// Tema de la app
const Theme = {
  dark: false,
  colors: {
    blanco: 'rgb(255, 255, 255)',
    rojo: 'red',
  },
};

// create a component
const navigatorContainer = () => {
  return (
    <NavigationContainer theme={Theme}>
      <Stack.Navigator initialRouteName={'Login'}>
        <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Home"
          component={Bottom}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

//make this component available to the app
export default navigatorContainer;
