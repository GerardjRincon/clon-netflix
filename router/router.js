export const LOGIN = 'Login'; // routa de login
export const HOME = 'Home'; // ruta para el home
export const NEW = 'New'; // ruta para los nuevos estrenos
export const LAUGHS = 'Laughs'; // ruta para risas
export const DOWNLOAD = 'Download'; // ruta para descargar
