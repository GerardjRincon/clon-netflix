//import liraries
import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Platform,
  TouchableOpacity,
  SafeAreaView,
  Vibration,
} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {BlurView} from '@react-native-community/blur';

// instancia de navigator
const Tab = createBottomTabNavigator();

///// importaciones de views
import {
  Home_stack,
  New_stack,
  Laughs_stack,
  Download_stack,
} from './navigatorStackApp';

//// component bottom
function MyTabBar({state, descriptors, navigation}) {
  return (
    <SafeAreaView
      style={{
        flexDirection: 'row',
        backgroundColor: '#202020',
        borderTopColor: '#202020',
        borderTopWidth: 1,
        height: Platform.OS === 'ios' ? hp('9%') : hp('6.5%'),
        width: '100%',
        position: 'absolute',
        bottom: 0,
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 5,
        },
        shadowOpacity: 0.36,
        shadowRadius: 6.68,
        elevation: 5,
        // justifyContent: 'space-around',
        alignItems: 'center',
      }}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const icon_true = options.icono_true;
        const icon_false = options.icono_false;
        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            // navigation.navigate(route.name, { screen: route.name });
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            activeOpacity={0.8}
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={() => {
              onPress(), Vibration.vibrate(100);
            }}
            onLongPress={onLongPress}
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            key={index}>
            <View
              style={{
                flex: 1,
                padding: 5,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  name={isFocused ? icon_true : icon_false}
                  size={hp('2.5%')}
                  color={isFocused ? 'white' : 'grey'}
                  style={{opacity: isFocused ? 1 : 0.5}}
                />
                <Text
                  style={{
                    color: isFocused ? 'white' : 'grey',
                    fontSize: hp('1.7%'),
                    opacity: isFocused ? 1 : 0.5,
                  }}>
                  {label}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        );
      })}
    </SafeAreaView>
  );
}

// bottom options
const Bottom = () => {
  return (
    <Tab.Navigator
      tabBar={props => <MyTabBar {...props} />}
      initialRouteName="HomeBottom"
      screenOptions={{
        activeTintColor: '#e33117',
      }}>
      <Tab.Screen
        name="HomeBottom"
        component={Home_stack}
        options={{
          tabBarLabel: 'Home',
          icono_true: 'home',
          icono_false: 'home-outline',
          headerShown: false,
        }}
      />
      <Tab.Screen
        name="NewBottom"
        component={New_stack}
        options={{
          tabBarLabel: 'New & Hot',
          icono_true: 'play-box-multiple',
          icono_false: 'play-box-multiple-outline',
          headerShown: false,
        }}
      />
      <Tab.Screen
        name="LaughsBottom"
        component={Laughs_stack}
        options={{
          tabBarLabel: 'Laughs',
          icono_true: 'emoticon-excited',
          icono_false: 'emoticon-excited-outline',
          headerShown: false,
        }}
      />
      <Tab.Screen
        name="DownloadBottom"
        component={Download_stack}
        options={{
          tabBarLabel: 'Download',
          icono_true: 'download-circle',
          icono_false: 'download-circle-outline',
          headerShown: false,
        }}
      />
    </Tab.Navigator>
  );
};

//make this component available to the app
export default Bottom;
